package br.eti.cvm.javatimeapi.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DateTimeDto {
	
	private LocalDate date;
	
	private LocalTime time;
	
	private LocalDateTime datetime;
	
}
