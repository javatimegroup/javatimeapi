package br.eti.cvm.javatimeapi.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.annotation.PostConstruct;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.eti.cvm.javatimeapi.dto.DateTimeDto;
import br.eti.cvm.javatimeapi.model.DateTime;
import br.eti.cvm.javatimeapi.repository.DateTimeRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping({"","/"})
public class IndexController {
	
	private final DateTimeRepository repository;
	
	private DateTimeDto dto;
	
	@PostConstruct
	private void init() {
		dto = DateTimeDto
				.builder()
				.date(LocalDate.now())
				.time(LocalTime.now())
				.datetime(LocalDateTime.now())
				.build();
		
		DateTime dt = DateTime
				.builder()
				.date(dto.getDate())
				.time(dto.getTime())
				.datetime(dto.getDatetime())
				.build();
		
		repository.save(dt);
	}
	
	@GetMapping
	public DateTimeDto index() {
		return dto;
	}
}
