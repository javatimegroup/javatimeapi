package br.eti.cvm.javatimeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavatimeapiApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(JavatimeapiApplication.class, args);
	}

}
