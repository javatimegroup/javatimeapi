package br.eti.cvm.javatimeapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.eti.cvm.javatimeapi.model.DateTime;

@Repository
public interface DateTimeRepository extends JpaRepository<DateTime, Long> {

}
