# JavaTimeApi

Aplicação web simples que mostra como os tipos de data e tempo da API Java Time são armazenados e exibidos.

## Saída no Navegador Web

![Exibe os dados no navegador web](site/json.png)

## H2 Console

![Exibe os dados armazenados no banco de dados H2.](site/h2_console.png)

## Referências

[Mapping Java 8 Date/Time Values](https://docs.jboss.org/hibernate/orm/5.3/userguide/html_single/Hibernate_User_Guide.html#basic-datetime-java8)

[Java SimpleDateFormat Is Not Simple](https://dzone.com/articles/java-simpledateformat-is-not-simple)

[Mapping Java 8 Date/Time entity attributes with Hibernate](https://in.relation.to/2018/02/20/java8-date-time-mapping/)

[ISO8601](https://en.wikipedia.org/wiki/ISO_8601)